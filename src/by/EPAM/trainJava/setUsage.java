package by.EPAM.trainJava;

import java.util.ArrayList;
import java.util.List;

public class setUsage {

	public static void main(String[] args) {
		Integer [] g ={1,2,3,4,5};
		// -------------         init
		CustomSet <Integer> k= new CustomSet <Integer>(5, g);
		System.out.println(k);
		// -------------         add
		k.add(10);
		// пробуем вставить существующий элемент
		System.out.println(k);
		if (!k.add(10)) {
			System.out.println("Коллекция содержит только уникальные");
		}
		// -------------         ins
		System.out.println(k);
		// вставим в 4ю позицию
		k.insert(7, 3);
		System.out.println(k);
		// -------------         del
		// удалим средний
		k.delete(4);
		System.out.println(k);
		// удалим последний
		k.delete(5);
		System.out.println(k);
		// -------------         addAll
		// добавим все из коллекции. элемент 7 уже есть - должен быть проигнорирован
		List <Integer> arL= new ArrayList<Integer>();
		arL.add(45);
		arL.add(59);
		arL.add(7);
		k.addAll(arL);
		System.out.println(k);
	}

}
