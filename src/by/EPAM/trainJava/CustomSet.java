package by.EPAM.trainJava;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/***
 * 
 * @author Sergii_Kotov
 * Реализуйте самостоятельно динамическую структуру Множество, 
 * не содержащую дублирующихся элементов. 
 * Используйте параметризацию при описании класса. 
 * (Условие не означает необходимости повторить все возможности класса HashSet).
 */
public class CustomSet <T> {
	private int volume;
    private Object[] cells;
    
    //---------------------------------------
    public CustomSet() {
    	this.volume = 0;
		this.cells = new Object [0];
    }
    
	public CustomSet(int volume, Object[] cells) {
		this.volume = volume;
		this.cells = cells;
	}

	public CustomSet(List <T> l) {
		this.volume = l.size();
		this.cells = l.toArray();
	}

	public CustomSet(Set <T> l) {
		this.volume = l.size();
		this.cells = l.toArray();
	}
	//---------------------------------------
	/***
	 * для проверки уникальности элемента
	 * @param n объект коллекции
	 * @return true - содержит
	 */
	public boolean contains(Object n) {
		if (n==null){
			return false;
		}
		for (Object b: cells) {
			if (n.equals(b))
	            return true;			
		}
		return false;
    }
	/***
	 * скопировать все
	 * @return true - удачно
	 */
    public Object[] copyToNew() {
        return Arrays.copyOf(cells, volume);
    }
	
    /***
     * добавим один элемент
     * @param o тип 
     * @return
     */
    public boolean add(T o) {
        if (!contains(o)) {
        	volume++;
        	cells=Arrays.copyOfRange(cells,0,volume);
        	cells[volume - 1] = o;
            return true;
        }
        return false;
    }

    /***
     * вставка элемента в множество
     * @param o элемент
     * @param i индекс элемента в множестве по которому будет вставлено
     * @return успешно?
     */
    public boolean insert(T o, int i) {
        if (!contains(o)) {
        	volume++;
        	// создаем новый массив на 1 длиннее режем на куски 
        	Object[] result = new Object [volume];
        	// сразу всунем что хотим 
        	result[i]=o;
        	// добавим часть до вставленного эл-та
        	System.arraycopy(Arrays.copyOfRange(cells, 0, i),0,result,0,i);
        	// добавим кончик 
        	System.arraycopy(Arrays.copyOfRange(cells, i,volume-1), 0, result, i+1, volume-i-1);
        	cells=result;
            return true;
        }
        return false;
    }

    /***
     * удаление элемента из множества
     * @param i индекс элемента в множестве
     * @return успешно?
     */
    public boolean delete(int i) {
        if ((i>=0)&&(i<volume)) {
        	volume--;
        	//Object[] ff = new Object[volume];
        	System.arraycopy(Arrays.copyOfRange(cells, i+1,volume+1), 0, cells, i, volume-i);
        	cells=Arrays.copyOf(cells, volume);
        	
            return true;
        }
        return false;
    }
    
    /***
     * удаление всех элементов
     */
    public void clear() {
    	// а достаточно ли просто удалить массив, без занулления его элементов? думаю да.
    	volume = 0;
		cells = new Object [0];
    }
    
    public List<Object> toList (){
    	return Arrays.asList(cells);
    }
    
    /***
     * дабавим сразу много. 
     * но с проверкой на уникальность результата!
     * @param v любая коллекция
     * @return удачно добавилось?
     */
    public boolean addAll (Collection<T> v){
    	try {
    		// пробежимся по колекции
    		Collection<T> vv= new ArrayList<T>();
    		vv.addAll(v);
    		for (T j:vv){
    			// удалим все совпадающие
    	        if (contains(j)) {
    	        	v.remove(j);
    	        }
    		}
    		vv.clear();
    		// оставшееся добавим в нашу
    		if (v.size()>0){
		    	volume+=v.size();
		    	Object[] result = new Object [volume];
		    	System.arraycopy(cells, 0, result, 0, volume-v.size());
		    	System.arraycopy(v.toArray(), 0, result, volume-v.size(), v.size());
		    	cells=result;
    		}
    	} catch (Throwable t) {
    		return false;
    	}
    	return true;
    }
    
    //---------------------------------------
    
    @Override
	public String toString() {
		return Arrays.toString(cells);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(cells);
		result = prime * result + volume;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomSet <T> other = (CustomSet<T>) obj;
		if (!Arrays.equals(cells, other.cells))
			return false;
		if (volume != other.volume)
			return false;
		return true;
	}
	
	public int getVolume() {
		return volume;
	}
		   
}
